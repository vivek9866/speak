import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicStorageModule } from '@ionic/storage';
import { FormsModule } from '@angular/forms'; 
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { AboutPage } from '../pages/about/about';
import { AboutUsPage } from '../pages/about-us/about-us';
import { SupportUsPage } from '../pages/support-us/support-us';
import { SuggestUsPage } from '../pages/suggest-us/suggest-us';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TextToSpeech  } from '@ionic-native/text-to-speech';
import { ServiceProvider } from '../providers/service/service';
import { HttpClientModule } from '@angular/common/http';
import { SuggestionProvider } from '../providers/suggestion/suggestion';
import { DatabaseProvider } from '../providers/database/database';

import { Pro } from '@ionic/pro';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    AboutUsPage,
    SupportUsPage,
    SuggestUsPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    AboutUsPage,
    SupportUsPage,
    SuggestUsPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    TextToSpeech ,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceProvider,
    SuggestionProvider,
    DatabaseProvider
  ]
})
export class AppModule {}
