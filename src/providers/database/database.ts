import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  private db: SQLiteObject;
  private isOpen: boolean;

  constructor(public http: HttpClient,public storage: SQLite) {
    console.log('Hello DatabaseProvider Provider');
    if (!this.isOpen) {
      this.storage = new SQLite();
      this.storage.create({ name: "data.db", location: "default" }).then((db: SQLiteObject) => {
        this.db = db;
        this.db.executeSql("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, text TEXT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)", []);
        this.isOpen = true;
      }).catch((error) => {
        console.log(error);
        alert(error);
      })
    }
  }

  AddText(text:string){
    return new Promise ((resolve, reject) => {
      let sql = "INSERT INTO users (text) VALUES (?)";
      this.db.executeSql(sql, [text]).then((data) =>{
        resolve(data);
        // alert(JSON.stringify(data));
      }, (error) => {
        reject(error);
        // alert(JSON.stringify(error));
      });
    });
  }

  GetAllItems(){
    return new Promise ((resolve, reject) => {
      this.db.executeSql("SELECT * FROM users", []).then((data) => {
        let arrayUsers = [];
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            arrayUsers.push({
              text: data.rows.item(i).text              
            });            
          }          
        }
        resolve(arrayUsers);
      }, (error) => {
        reject(error);
      })
    })
  }

}
