import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { AboutUsPage } from '../../pages/about-us/about-us';
import { SupportUsPage } from '../../pages/support-us/support-us';
import { SuggestUsPage } from '../../pages/suggest-us/suggest-us';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController,public modalCtrl: ModalController) {

  }

  openAboutUS():void {
    let profileModal = this.modalCtrl.create(AboutUsPage);
    profileModal.present();
  }

  openSupportUS():void {
    let profileModal = this.modalCtrl.create(SupportUsPage);
    profileModal.present();
  }
  openSuggestUS():void {
    let profileModal = this.modalCtrl.create(SuggestUsPage);
    profileModal.present();
  }
 
}
