import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
 items:any;
  constructor(public navCtrl: NavController, private platform: Platform,
              public storage: Storage, private tts: TextToSpeech, private database : DatabaseProvider,) {
  // storage.get('speakOutLoudLikeALegend').then((val) => {
  //     console.log('Your age is', val);
  //     this.items = val;
  //   });

    // this.database.GetAllItems().then((data) => {
    //   console.log(data);
    //   alert(JSON.stringify(data));
    //   this.items = data;
    // }, (error) => {
    //   console.log(error);
    //   alert(JSON.stringify(error));
    // })
  }

  ionViewDidEnter() {
    setTimeout(() => {
      // this.storage.get('speakOutLoudLikeALegend').then((val) => {
      //   this.items = val;
      // }); 

      this.database.GetAllItems().then((data) => {
        console.log(data);
        // alert(JSON.stringify(data));
        this.items = data;
      }, (error) => {
        console.log(error);
        // alert(JSON.stringify(error));
      });
    }, 500);
    }

    itemSelected(event){
      this.tts.speak(event)
      .then(() => console.log('Success'))
      .catch((reason: any) => console.log(reason));
    }
    
}