import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormsModule } from '@angular/forms';
import { SuggestionProvider } from '../../providers/suggestion/suggestion';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the SuggestUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-suggest-us',
  templateUrl: 'suggest-us.html',
})
export class SuggestUsPage {

  todo:any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public formsModule: FormsModule, public suggestionSvs: SuggestionProvider,
              public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuggestUsPage');
  }

  closeModal():void {
    this.navCtrl.pop();
  } 

  logForm():void {
    console.log(this.todo);
    this.suggestionSvs.postData(this.todo).subscribe( data => {	
      let alert = this.alertCtrl.create({
        title: 'Suggestion Submitted',
        subTitle: 'We Value your sugggestion, Thanks for helping us in right direction',
        buttons: ['OK']
      });
      alert.present();			   
      },
      error => { 
        console.log(error);
        let alert = this.alertCtrl.create({
          title: 'Suggestion Not Submitted',
          subTitle: 'We Value your sugggestion, Mail your suggestion at nanda.hcja@gmail.com',
          buttons: ['OK']
        });
        alert.present();	
      });
  }

}
