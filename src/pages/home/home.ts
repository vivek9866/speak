import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
import { DatabaseProvider } from '../../providers/database/database'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [ServiceProvider]
})
export class HomePage {
  toggleStatus;
  gender:string = 'woman';
  icon:string = 'woman';
  constructor(public navCtrl: NavController,
              private tts: TextToSpeech,
              private sqlite: SQLite,
              private storage: Storage,
              private database : DatabaseProvider,
              private serviceProvider: ServiceProvider) {

    this.storage.get('gender').then((val) => {  
      this.gender = val;
      this.icon = val;
    }); 

    //  this.storage.get('speakOutLoudLikeALegend').then((val:any) => {
    //     // storage.set('speakOutLoudLikeALegend',['Hello']);
    // },(val:any) => {
    //   this.storage.set('speakOutLoudLikeALegend',['Hello']);
    // }); 
  }

  sendText(event){
    console.log(event);
    // this.storage.get('speakOutLoudLikeALegend').then((val:any) => {
    //   val.push(event._value); 
    //   this.storage.set('speakOutLoudLikeALegend',val);
    // }); 

    this.database.AddText(event._value).then( (data) => {
      console.log(data);
    }, (error) => {
      console.log(error);
      alert(error);
    })

    this.tts.speak({text: event._value,
      locale: 'en-US',
      rate: 1})
    .then(() => console.log('Success'))
    .catch((reason: any) => console.log(reason));
  }

  Change_Toggle() {
    if(this.toggleStatus == true){
      this.gender = 'man';
      this.icon = 'man';
      this.storage.set('gender','man');
    } else {
      this.gender = 'woman';
      this.icon = 'woman';
      this.storage.set('gender','woman');
     }
  }

}
